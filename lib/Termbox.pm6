unit module Termbox;

use NativeCall;

# Key constants. See also Event's key field.
#
# These are a safe subset of terminfo keys, which exist on all popular
# terminals. Termbox uses only them to stay truly portable.

my constant TB_KEY_F1                is export(:keys) = 0xFFFF -  0;
my constant TB_KEY_F2                is export(:keys) = 0xFFFF -  1;
my constant TB_KEY_F3                is export(:keys) = 0xFFFF -  2;
my constant TB_KEY_F4                is export(:keys) = 0xFFFF -  3;
my constant TB_KEY_F5                is export(:keys) = 0xFFFF -  4;
my constant TB_KEY_F6                is export(:keys) = 0xFFFF -  5;
my constant TB_KEY_F7                is export(:keys) = 0xFFFF -  6;
my constant TB_KEY_F8                is export(:keys) = 0xFFFF -  7;
my constant TB_KEY_F9                is export(:keys) = 0xFFFF -  8;
my constant TB_KEY_F10               is export(:keys) = 0xFFFF -  9;
my constant TB_KEY_F11               is export(:keys) = 0xFFFF - 10;
my constant TB_KEY_F12               is export(:keys) = 0xFFFF - 11;
my constant TB_KEY_INSERT            is export(:keys) = 0xFFFF - 12;
my constant TB_KEY_DELETE            is export(:keys) = 0xFFFF - 13;
my constant TB_KEY_HOME              is export(:keys) = 0xFFFF - 14;
my constant TB_KEY_END               is export(:keys) = 0xFFFF - 15;
my constant TB_KEY_PGUP              is export(:keys) = 0xFFFF - 16;
my constant TB_KEY_PGDN              is export(:keys) = 0xFFFF - 17;
my constant TB_KEY_ARROW_UP          is export(:keys) = 0xFFFF - 18;
my constant TB_KEY_ARROW_DOWN        is export(:keys) = 0xFFFF - 19;
my constant TB_KEY_ARROW_LEFT        is export(:keys) = 0xFFFF - 20;
my constant TB_KEY_ARROW_RIGHT       is export(:keys) = 0xFFFF - 21;
my constant TB_KEY_MOUSE_LEFT        is export(:keys) = 0xFFFF - 22;
my constant TB_KEY_MOUSE_RIGHT       is export(:keys) = 0xFFFF - 23;
my constant TB_KEY_MOUSE_MIDDLE      is export(:keys) = 0xFFFF - 24;
my constant TB_KEY_MOUSE_RELEASE     is export(:keys) = 0xFFFF - 25;
my constant TB_KEY_MOUSE_WHEEL_UP    is export(:keys) = 0xFFFF - 26;
my constant TB_KEY_MOUSE_WHEEL_DOWN  is export(:keys) = 0xFFFF - 27;

# These are all ASCII code points below SPACE character and a BACKSPACE key.

my constant TB_KEY_CTRL_TILDE        is export(:keys) = 0x00;
my constant TB_KEY_CTRL_2            is export(:keys) = 0x00; # clash with 'CTRL_TILDE'
my constant TB_KEY_CTRL_A            is export(:keys) = 0x01;
my constant TB_KEY_CTRL_B            is export(:keys) = 0x02;
my constant TB_KEY_CTRL_C            is export(:keys) = 0x03;
my constant TB_KEY_CTRL_D            is export(:keys) = 0x04;
my constant TB_KEY_CTRL_E            is export(:keys) = 0x05;
my constant TB_KEY_CTRL_F            is export(:keys) = 0x06;
my constant TB_KEY_CTRL_G            is export(:keys) = 0x07;
my constant TB_KEY_BACKSPACE         is export(:keys) = 0x08;
my constant TB_KEY_CTRL_H            is export(:keys) = 0x08; # clash with 'CTRL_BACKSPACE'
my constant TB_KEY_TAB               is export(:keys) = 0x09;
my constant TB_KEY_CTRL_I            is export(:keys) = 0x09; # clash with 'TAB'
my constant TB_KEY_CTRL_J            is export(:keys) = 0x0A;
my constant TB_KEY_CTRL_K            is export(:keys) = 0x0B;
my constant TB_KEY_CTRL_L            is export(:keys) = 0x0C;
my constant TB_KEY_ENTER             is export(:keys) = 0x0D;
my constant TB_KEY_CTRL_M            is export(:keys) = 0x0D; # clash with 'ENTER'
my constant TB_KEY_CTRL_N            is export(:keys) = 0x0E;
my constant TB_KEY_CTRL_O            is export(:keys) = 0x0F;
my constant TB_KEY_CTRL_P            is export(:keys) = 0x10;
my constant TB_KEY_CTRL_Q            is export(:keys) = 0x11;
my constant TB_KEY_CTRL_R            is export(:keys) = 0x12;
my constant TB_KEY_CTRL_S            is export(:keys) = 0x13;
my constant TB_KEY_CTRL_T            is export(:keys) = 0x14;
my constant TB_KEY_CTRL_U            is export(:keys) = 0x15;
my constant TB_KEY_CTRL_V            is export(:keys) = 0x16;
my constant TB_KEY_CTRL_W            is export(:keys) = 0x17;
my constant TB_KEY_CTRL_X            is export(:keys) = 0x18;
my constant TB_KEY_CTRL_Y            is export(:keys) = 0x19;
my constant TB_KEY_CTRL_Z            is export(:keys) = 0x1A;
my constant TB_KEY_ESC               is export(:keys) = 0x1B;
my constant TB_KEY_CTRL_LSQ_BRACKET  is export(:keys) = 0x1B; # clash with 'ESC'
my constant TB_KEY_CTRL_3            is export(:keys) = 0x1B; # clash with 'ESC'
my constant TB_KEY_CTRL_4            is export(:keys) = 0x1C;
my constant TB_KEY_CTRL_BACKSLASH    is export(:keys) = 0x1C; # clash with 'CTRL_4'
my constant TB_KEY_CTRL_5            is export(:keys) = 0x1D;
my constant TB_KEY_CTRL_RSQ_BRACKET  is export(:keys) = 0x1D; # clash with 'CTRL_5'
my constant TB_KEY_CTRL_6            is export(:keys) = 0x1E;
my constant TB_KEY_CTRL_7            is export(:keys) = 0x1F;
my constant TB_KEY_CTRL_SLASH        is export(:keys) = 0x1F; # clash with 'CTRL_7'
my constant TB_KEY_CTRL_UNDERSCORE   is export(:keys) = 0x1F; # clash with 'CTRL_7'
my constant TB_KEY_SPACE             is export(:keys) = 0x20;
my constant TB_KEY_BACKSPACE2        is export(:keys) = 0x7F;
my constant TB_KEY_CTRL_8            is export(:keys) = 0x7F; # clash with 'BACKSPACE2'

# Alt modifier constant, see Event.mod field and tb_select_input_mode function.
# Mouse-motion modifier

my constant TB_MOD_ALT               is export(:keys) = 0x01;
my constant TB_MOD_MOTION            is export(:keys) = 0x02;

# Colors (see Cell's fg and bg fields)

my constant TB_DEFAULT               is export(:styles) = 0x00;
my constant TB_BLACK                 is export(:styles) = 0x01;
my constant TB_RED                   is export(:styles) = 0x02;
my constant TB_GREEN                 is export(:styles) = 0x03;
my constant TB_YELLOW                is export(:styles) = 0x04;
my constant TB_BLUE                  is export(:styles) = 0x05;
my constant TB_MAGENTA               is export(:styles) = 0x06;
my constant TB_CYAN                  is export(:styles) = 0x07;
my constant TB_WHITE                 is export(:styles) = 0x08;

# Attributes, it is possible to use multiple attributes by combining them
# using bitwise OR ('+|'). Although, colors cannot be combined. But you can
# combine attributes and a single color. See also Cell's fg and bg
# fields.

my constant TB_BOLD                  is export(:styles) = 0x0100;
my constant TB_UNDERLINE             is export(:styles) = 0x0200;
my constant TB_REVERSE               is export(:styles) = 0x0400;

# A cell, single conceptual entity on the terminal screen. The terminal screen
# is basically a 2d array of cells. It has the following fields:
#  - 'ch' is a unicode character
#  - 'fg' foreground color and attributes
#  - 'bg' background color and attributes
class Cell is repr('CStruct') {
    has uint32 $.ch is rw;
    has uint16 $.fg is rw;
    has uint16 $.bg is rw;
}

my constant TB_EVENT_KEY             is export(:events) = 1;
my constant TB_EVENT_RESIZE          is export(:events) = 2;
my constant TB_EVENT_MOUSE           is export(:events) = 3;

# An event, single interaction from the user. The 'mod' and 'ch' fields are
# valid if 'type' is TB_EVENT_KEY. The 'w' and 'h' fields are valid if 'type'
# is TB_EVENT_RESIZE. The 'x' and 'y' fields are valid if 'type' is
# TB_EVENT_MOUSE. The 'key' field is valid if 'type' is either TB_EVENT_KEY
# or TB_EVENT_MOUSE. The fields 'key' and 'ch' are mutually exclusive; only
# one of them can be non-zero at a time.
class Event is repr('CStruct') {
    has uint8  $.type;
    has uint8  $.mod;
    has uint16 $!key;
    has uint32 $.ch;
    has int32  $.w;
    has int32  $.h;
    has int32  $.x;
    has int32  $.y;

    method key () { $!key < 0 ?? $!key + 0xFFFF + 1 !! $!key }
}

# Error codes returned by tb_init(). All of them are self-explanatory, except
# the pipe trap error. Termbox uses unix pipes in order to deliver a message
# from a signal handler (SIGWINCH) to the main event reading loop. Honestly in
# most cases you should just check the returned code as < 0.

my constant TB_EUNSUPPORTED_TERMINAL is export(:errors) = -1;
my constant TB_EFAILED_TO_OPEN_TTY   is export(:errors) = -2;
my constant TB_EPIPE_TRAP_ERROR      is export(:errors) = -3;

my constant TB_HIDE_CURSOR           is export(:modes) = -1;

my constant TB_INPUT_CURRENT         is export(:modes) = 0; # 000
my constant TB_INPUT_ESC             is export(:modes) = 1; # 001
my constant TB_INPUT_ALT             is export(:modes) = 2; # 010
my constant TB_INPUT_MOUSE           is export(:modes) = 4; # 100

my constant TB_OUTPUT_CURRENT        is export(:modes) = 0;
my constant TB_OUTPUT_NORMAL         is export(:modes) = 1;
my constant TB_OUTPUT_256            is export(:modes) = 2;
my constant TB_OUTPUT_216            is export(:modes) = 3;
my constant TB_OUTPUT_GRAYSCALE      is export(:modes) = 4;

# Using int8 / int16 instead of plain int

my constant TERMBOX = %?RESOURCES<libraries/termbox>;

sub tb-init ( --> int8 )
    is native(TERMBOX) is export(:subs) is symbol('tb_init') {*}

sub tb-shutdown ()
    is native(TERMBOX) is export(:subs) is symbol('tb_shutdown') {*}

sub tb-width ( --> int16 )
    is native(TERMBOX) is export(:subs) is symbol('tb_width') {*}

sub tb-height ( --> int16 )
    is native(TERMBOX) is export(:subs) is symbol('tb_height') {*}

sub tb-clear ()
    is native(TERMBOX) is export(:subs) is symbol('tb_clear') {*}

sub tb-present ()
    is native(TERMBOX) is export(:subs) is symbol('tb_present') {*}

sub tb-put-cell ( int16 $x, int16 $y, Cell $c )
    is native(TERMBOX) is export(:subs) is symbol('tb_put_cell') {*}

sub tb-change-cell ( int16 $x, int16 $y, uint32 $ch, uint16 $fg, uint16 $bg )
    is native(TERMBOX) is export(:subs) is symbol('tb_change_cell') {*}

sub tb-blit ( int16 $x, int16 $y, int16 $w, int16 $h, Cell $c )
    is native(TERMBOX) is export(:subs) is symbol('tb_blit') {*}

sub tb-cell-buffer( --> Pointer[Cell] )
    is native(TERMBOX) is export(:subs) is symbol('tb_cell_buffer') {*}

sub tb-select-input-mode ( int8 $mode --> int8 )
    is native(TERMBOX) is export(:subs) is symbol('tb_select_input_mode') {*}

sub tb-select-output-mode ( int8 $mode --> int8 )
    is native(TERMBOX) is export(:subs) is symbol('tb_select_output_mode') {*}

sub tb-peek-event ( Event $e is rw, int8 $timeout --> int8 )
    is native(TERMBOX) is export(:subs) is symbol('tb_peek_event') {*}

sub tb-poll-event ( Event $e is rw --> int8 )
    is native(TERMBOX) is export(:subs) is symbol('tb_poll_event') {*}

# UTF-8 helpers

sub tb-utf8-char-to-unicode ( uint32 $out is rw, Str $in --> int8 )
    is native(TERMBOX) is export(:subs) is symbol('tb_utf8_char_to_unicode') {*}

sub tb-utf8-unicode-to-char ( CArray[uint8] $out is rw, uint32 $in --> int8 )
    is native(TERMBOX) is export(:subs) is symbol('tb_utf8_unicode_to_char') {*}

# UTF-8 wrappers

sub tb-encode-string ( Str $in where *.chars > 0 --> Int ) is export(:subs) {
    my $ret = tb-utf8-char-to-unicode( my uint32 $uint, $in );
    die $ret if $ret < 0;
    $uint.Int;
}

sub tb-decode-string ( Int $in where * > 1 --> Str ) is export(:subs) {
    my $char = CArray[uint8].allocate(4);
    my $len  = tb-utf8-unicode-to-char( $char, $in );
    die $len if $len < 0;
    Blob.new( $char[ ^$len ] ).decode;
}

# This module should export nothing by default. All symbols are available
# inside the Termbox:: package, either as constants of the form
# Termbox::CONSTANT_NAME or subs of the form Termbox::sub-name.
#
# If the caller chooses to import symbols using any of the defined tags,
# then these are exported with a prefix, to avoid possible namespace clashes.
# Constants should be exported as TB_CONSTANT_NAME, while subs should be
# exported as tb-sub-name.
#
# Constant names cannot use KEBAB-CASE because of constants like KEY_CTRL_2
# which would result in an illegal identifier.
#
# The block below takes care of this name transformation.
for EXPORT::ALL::.kv -> $k, $v {
    OUR::{ S/ ^ ('&'?) [ 'tb-' | 'TB_' ] /$0/ given $k } = $v;
}
