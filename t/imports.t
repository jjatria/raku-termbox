#!/usr/bin/env raku

use Test;

subtest 'Import tags' => {
    use Termbox;
    is-deeply Termbox::EXPORT::.keys.Set,
        < events subs keys modes errors ALL styles >.Set,
        'Defines the right set of impor tags';
}

subtest 'Plain import' => {
    my @before = MY::.keys;
    my @after  = do { use Termbox; MY::.keys };

    is-deeply ( @after (-) @before ), < Termbox >.Set,
        'Plain use only imports Termbox package';

    is-deeply do { use Termbox; Termbox::.keys.Set }, <
        EXPORT

        &cell-buffer           &change-cell    &init       &blit
        &select-input-mode     &encode-string  &put-cell   &shutdown
        &select-output-mode    &decode-string  &present    &width
        &utf8-char-to-unicode  &poll-event     &height     &clear
        &utf8-unicode-to-char  &peek-event

        BLACK  BLUE  BOLD  CYAN  GREEN  MAGENTA  RED  WHITE  YELLOW

        Cell Event

        EFAILED_TO_OPEN_TTY  EPIPE_TRAP_ERROR  EUNSUPPORTED_TERMINAL
        EVENT_KEY            EVENT_MOUSE       EVENT_RESIZE

        INPUT_ALT  INPUT_CURRENT  INPUT_ESC  INPUT_MOUSE

        DEFAULT
        HIDE_CURSOR
        MOD_ALT
        MOD_MOTION
        REVERSE
        UNDERLINE

        OUTPUT_216        OUTPUT_256    OUTPUT_CURRENT
        OUTPUT_GRAYSCALE  OUTPUT_NORMAL

        KEY_ARROW_DOWN KEY_ARROW_LEFT KEY_ARROW_RIGHT KEY_ARROW_UP

        KEY_MOUSE_LEFT   KEY_MOUSE_MIDDLE      KEY_MOUSE_RELEASE
        KEY_MOUSE_RIGHT  KEY_MOUSE_WHEEL_DOWN  KEY_MOUSE_WHEEL_UP

        KEY_BACKSPACE         KEY_CTRL_L              KEY_END
        KEY_BACKSPACE2        KEY_CTRL_LSQ_BRACKET    KEY_ENTER
        KEY_CTRL_2            KEY_CTRL_M              KEY_ESC
        KEY_CTRL_3            KEY_CTRL_N              KEY_F1
        KEY_CTRL_4            KEY_CTRL_O              KEY_F10
        KEY_CTRL_5            KEY_CTRL_P              KEY_F11
        KEY_CTRL_6            KEY_CTRL_Q              KEY_F12
        KEY_CTRL_7            KEY_CTRL_R              KEY_F2
        KEY_CTRL_8            KEY_CTRL_RSQ_BRACKET    KEY_F3
        KEY_CTRL_A            KEY_CTRL_S              KEY_F4
        KEY_CTRL_B            KEY_CTRL_SLASH          KEY_F5
        KEY_CTRL_BACKSLASH    KEY_CTRL_T              KEY_F6
        KEY_CTRL_C            KEY_CTRL_TILDE          KEY_F7
        KEY_CTRL_D            KEY_CTRL_U              KEY_F8
        KEY_CTRL_E            KEY_CTRL_UNDERSCORE     KEY_F9
        KEY_CTRL_F            KEY_CTRL_V              KEY_HOME
        KEY_CTRL_G            KEY_CTRL_W              KEY_INSERT
        KEY_CTRL_H            KEY_CTRL_X              KEY_PGDN
        KEY_CTRL_I            KEY_CTRL_Y              KEY_PGUP
        KEY_CTRL_J            KEY_CTRL_Z              KEY_SPACE
        KEY_CTRL_K            KEY_DELETE              KEY_TAB
    >.Set, 'Plain use exposes correct symbols in Termbox';
}

subtest 'Import tags' => {
    my %imports = (
        styles => <
            TB_DEFAULT TB_BLACK TB_RED   TB_GREEN TB_YELLOW    TB_BLUE
            TB_MAGENTA TB_CYAN  TB_WHITE TB_BOLD  TB_UNDERLINE TB_REVERSE
            >.Set,
        events => < TB_EVENT_KEY TB_EVENT_RESIZE TB_EVENT_MOUSE >.Set,
        errors => <
            TB_EUNSUPPORTED_TERMINAL TB_EFAILED_TO_OPEN_TTY TB_EPIPE_TRAP_ERROR
            >.Set,
        modes => <
            TB_HIDE_CURSOR TB_INPUT_CURRENT    TB_INPUT_ESC     TB_INPUT_ALT
            TB_INPUT_MOUSE TB_OUTPUT_CURRENT   TB_OUTPUT_NORMAL TB_OUTPUT_256
            TB_OUTPUT_216  TB_OUTPUT_GRAYSCALE
            >.Set,
        subs => <
            &tb-cell-buffer            &tb-change-cell    &tb-init      &tb-blit
            &tb-select-input-mode      &tb-decode-string  &tb-put-cell  &tb-shutdown
            &tb-select-output-mode     &tb-encode-string  &tb-present   &tb-width
            &tb-utf8-char-to-unicode   &tb-poll-event     &tb-height    &tb-clear
            &tb-utf8-unicode-to-char   &tb-peek-event
            >.Set,
        keys => <
            TB_KEY_F1      TB_KEY_CTRL_C  TB_KEY_CTRL_3
            TB_KEY_F2      TB_KEY_CTRL_D  TB_KEY_CTRL_4
            TB_KEY_F3      TB_KEY_CTRL_E  TB_KEY_CTRL_5
            TB_KEY_F4      TB_KEY_CTRL_F  TB_KEY_CTRL_6
            TB_KEY_F5      TB_KEY_CTRL_G  TB_KEY_CTRL_7
            TB_KEY_F6      TB_KEY_CTRL_H  TB_KEY_CTRL_8
            TB_KEY_F7      TB_KEY_CTRL_I  TB_MOD_MOTION
            TB_KEY_F8      TB_KEY_CTRL_J  TB_KEY_ARROW_UP
            TB_KEY_F9      TB_KEY_CTRL_K  TB_KEY_BACKSPACE
            TB_KEY_F10     TB_KEY_CTRL_L  TB_KEY_BACKSPACE2
            TB_KEY_F11     TB_KEY_CTRL_M  TB_KEY_CTRL_SLASH
            TB_KEY_F12     TB_KEY_CTRL_N  TB_KEY_CTRL_TILDE
            TB_MOD_ALT     TB_KEY_CTRL_O  TB_KEY_ARROW_DOWN
            TB_KEY_END     TB_KEY_CTRL_P  TB_KEY_ARROW_LEFT
            TB_KEY_ESC     TB_KEY_CTRL_Q  TB_KEY_MOUSE_LEFT
            TB_KEY_TAB     TB_KEY_CTRL_R  TB_KEY_ARROW_RIGHT
            TB_KEY_HOME    TB_KEY_CTRL_S  TB_KEY_MOUSE_RIGHT
            TB_KEY_PGDN    TB_KEY_CTRL_T  TB_KEY_MOUSE_MIDDLE
            TB_KEY_PGUP    TB_KEY_CTRL_U  TB_KEY_MOUSE_RELEASE
            TB_KEY_ENTER   TB_KEY_CTRL_V  TB_KEY_MOUSE_WHEEL_UP
            TB_KEY_SPACE   TB_KEY_CTRL_W  TB_KEY_CTRL_BACKSLASH
            TB_KEY_DELETE  TB_KEY_CTRL_X  TB_KEY_CTRL_UNDERSCORE
            TB_KEY_INSERT  TB_KEY_CTRL_Y  TB_KEY_MOUSE_WHEEL_DOWN
            TB_KEY_CTRL_A  TB_KEY_CTRL_Z  TB_KEY_CTRL_LSQ_BRACKET
            TB_KEY_CTRL_B  TB_KEY_CTRL_2  TB_KEY_CTRL_RSQ_BRACKET
            >.Set,
    );

    for %imports.kv -> $tag, $expected {
        is-deeply do { EVAL "use Termbox :{ $tag }; MY::.keys.grep(/ 'TB_' | 'tb-' /).Set" },
            $expected, "Importing :{ $tag } imports correct names";
    }
}

done-testing;
