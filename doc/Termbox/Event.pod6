=begin pod

=head2 NAME

Termbox::Event - A single interaction from the user

=head2 SYNOPSIS

=begin code

use Termbox :ALL;

tb-init;
END tb-shutdown;

while tb-poll-event( my $e = Termbox::Event.new ) {
    my $x = 0;
    for $e.gist.comb.map: *.&tb-encode-string {
        tb-change-cell( $x++, 0, $_, 0, 0 );
        LAST tb-present;
    }

    if $e.type == TB_EVENT_KEY && $e.key == TB_KEY_ESC {
        # Pressed ESC. Stopping...
        last;
    }
}

=end code

=head2 DESCRIPTION

An event, single interaction from the user.

=head2 METHODS

=head3 type

Returns the ype of this event, as one of the constants exported by C<Termbox>
with the C<event> tag (eg. C<TB_EVENT_KEY> or C<Termbox::EVENT_KEY>).

Use this to determine which of the remaining methods of this class to use, since
some of them will only produce meaningful results for certain kinds of events.

=begin table
 Type            | key | ch | mod | x | y | w | h
 =================================================
 C<EVENT_KEY>    |  o  | o  |  o  |   |   |   |
 C<EVENT_RESIZE> |     |    |     |   |   | o | o
 C<EVENT_MOUSE>  |  o  |    |     | o | o |   |
=end table

=head3 key

For events of type C<EVENT_KEY>, returns the key code that was pressed
(eg. C<KEY_F1>). If this returns a 0 value, use the C<ch> method instead.

This is only valid if C<ch> returns a 0 value.

=head3 ch

For events of type C<EVENT_KEY>, returns the character that was pressed.
If this returns a 0 value, use the C<key> method instead.


This is only valid if C<key> returns a 0 value.

=head3 mod

For events of type C<EVENT_KEY>, returns the modifier for the key (eg. C<MOD_ALT>).

=head3 x

For events of type C<EVENT_MOUSE>, returns the column under the mouse cursor.

=head3 y

For events of type C<EVENT_MOUSE>, returns the row under the mouse cursor.

=head3 w

For events of type C<EVENT_RESIZE>, returns the new terminal width.

=head3 h

For events of type C<EVENT_RESIZE>, returns the new terminal height.

=head1 AUTHOR

José Joaquín Atria <jjatria@cpan.org>

=head1 COPYRIGHT AND LICENSE

Copyright 2020 José Joaquín Atria

This library is free software; you can redistribute it and/or modify it under
the Artistic License 2.0.

=end pod
